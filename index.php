<?php require_once 'header.php'; ?>

<section id="intro">

  <div class="jumbotron jumbotron-fluid intro" id="jumbotron">

    <!-- intro text -->
    <div class="intro-wrapper">

      <div class="container rev" id="head-text" style="margin-top: 200px">
        <center><h1>Accelerate investors and entrepreneurs’ social and financial mission</h1>
          <a href="http://unltd-indonesia.org/"> <img src="images/logo-unltd.png" alt=""></a>
          <a href="http://instellar.id"> <img src="images/logo.png" alt=""> </a>
        </center>
      </div>

    </div> <!-- ./intro-wrapper -->

  </div> <!-- jumbotron -->

</section> <!-- /.intro -->


<section id="about">
  <!-- _wrapper hanya class -->
  <div class="_wrapper" style="margin: 0 auto; width: 100%; background:#b89f86">
    <div class="row" style="margin:0; max-width:100%">
      <!-- video profil perusahaan -->
      <div class="video">
        <img src="images/video-scr.jpg" alt="" >
      </div>
      <!-- about 1 -->
      <div class=" about">
        <!-- <h3>About Us</h3>
        <div class="divider rev"></div> -->
        <p><span>EMPASSION is an impact investment advisory,
            management and talent consulting that optimizes
            resources - financial, skills and networks - to
            accelerate investors and entrepreneurs’ social and
            financial mission.</span><br><br>
            We strongly hold the values of trust, respect,
            integrity and hands-on in delivering our services.<br><br>
            We affiliates with Unltd Indonesia and Instellar that
            have been incubated and accelerated 75+
            sustainable and scallable social enterprises in
            Indonesia.</p>
      </div> <!-- /.about -->
    </div>  <!-- /.row -->
  </div> <!-- /._wrapper -->


  <!-- about 2 -->
  <div class="container about-sc rev">
    <center> <h2>Our Value Added</h2>
    <div class="divider"></div></center>

    <div class="row foo" style="width:100%; margin-left:0px !important;">
      <div class="col-sm-3 rev">
        <img src="images/ova1.png" alt=""> <br>
        <strong>Business, Financial and Investment expert</strong>
        <p>We are led by passionate experts with more than 60 years of
        combined experiences in the field of social entrepreneurship, finance
        investment, business turnaround, training, incubaton and
        accelelator program.</p>.<br><br>
      </div>
      <div class="col-sm-3 rev">

          <img src="images/ova2.png" alt=""> <br>
          <strong>Trusted track records in ecosystem</strong>
          <p>We have a deep understanding and strong trust with all stakeholders
          in the ecosystems.</p><br><br>

      </div>
      <div class="col-sm-3 rev">

          <img src="images/ova3.png" alt=""> <br>
          <strong>Wide range of ecosystem networks</strong>
          <p>We have built a wide range of networks ranging from social
          enterprises, NGOs, small to large corporates, investors and
          philanthropist.</p>

      </div>
      <div class="col-sm-3 rev">

          <img src="images/ova4.png" alt=""> <br>
          <strong>Investor and entrepreneurial mindset</strong><br>
          <p>We understand key investment priorities required by investors. We
          provide a straight forward analysis and insights which has the most
          significant impact to the investment considerations.</p>

      </div>
    </div>
        <!-- <p>
          <strong>Business, Financial and Investment expert</strong><br>
          We are led by passionate experts with more than 60 years of
          combined experiences in the field of social entrepreneurship,
          investment, business turnaround, training, incubaton and
          accelelator program.<br><br>
          <strong>Trusted track records in social enterprises ecosystem</strong><br>
          We have a deep understanding and stong trust with all stakeholders
          in the ecosystems.<br><br>
          <strong>Wide range of networks</strong><br>
          We have built a wide range of networks ranging from social
          enterprises, NGOs, small to large corporates, investors and
          philanthropist.<br><br>
          <strong>Investor and entrepreneurial mindset</strong><br>
          We understand key investment priorities required by investors. We
          provide a straight forward analysis and insights which has the most
          significant impact to the investment considerations.</p> -->
    </div> <!-- /.about-sc -->

    <div class="about-last">
      <div class="about-last-text">
        <br>  <center> <h2>Filling The Gap</h2>
          <div class="divider2"></div><br>
        <p>
          EMPASSION believes sustainable development goals can be achieved
          faster by balancing financial returns and measurable economic, social and
          environmental values within all stakeholders.
          We create a trusted and hands-on impact advisory to fill the gap
          between the investors and social enterprises
        </p></center>
      </div>
    </div>

    <div class="gallery">

          <div class="left-gallery">
            <img src="images/photo-socent/DSC_7428.JPG">
            <img src="images/photo-socent/F57C6855.JPG">
          </div>

          <div class="mid">
             <div class="carousel js-flickity">
               <img src="images/photo-socent/DSC_7800.JPG">
               <img src="images/photo-socent/DSC_7803.JPG">
               <img src="images/photo-socent/DSC_7428.JPG">
               <img src="images/photo-socent/DSC_7477.JPG">
               <img src="images/photo-socent/DSC_7674.JPG">
               <img src="images/photo-socent/DSC_7626.JPG">
               <img src="images/photo-socent/DSC_7783.JPG">
               <img src="images/photo-socent/DSC_7764.JPG">
            </div>
          </div>

          <div class="right-gallery">
            <img src="images/photo-socent/F57C8523.JPG">
            <img src="images/photo-socent/F57C8315.JPG">
          </div>
     
       <!--  <div><img class="big" src="images/photo-socent/DSC_7476.JPG" alt=""></div>
        <div><img src="images/photo-socent/DSC_7417.JPG" alt=""></div>
        <div><img src="images/photo-socent/DSC_7428.JPG" alt=""></div>
        <div><img src="images/photo-socent/DSC_7432.JPG" alt=""></div>
        <div><img src="images/photo-socent/DSC_7626.JPG" alt=""></div>
        <div><img src="images/photo-socent/DSC_7432.JPG" alt=""></div>
        <div><img src="images/photo-socent/DSC_7626.JPG" alt=""></div>
        <div><img src="images/photo-socent/DSC_7651.JPG" alt=""></div>
        <div><img src="images/photo-socent/DSC_7654.JPG" alt=""></div>
        <div><img src="images/photo-socent/DSC_7658.JPG" alt=""></div>
        <div><img src="images/photo-socent/DSC_7761.JPG" alt=""></div>
        <div><img src="images/photo-socent/DSC_7476.JPG" alt=""></div>
        <div><img src="images/photo-socent/DSC_7477.JPG" alt=""></div>
        <div><img src="images/photo-socent/DSC_7496.JPG" alt=""></div>
        <div><img src="images/photo-socent/DSC_7614.JPG" alt=""></div>
        <div><img src="images/photo-socent/DSC_7661.JPG" alt=""></div>
        <div><img src="images/photo-socent/DSC_7662.JPG" alt=""></div> -->
    </div>
</section> <!-- /#about -->



<!-- SERVICES -->
<section id="services" style="background: #d3d7ab; width:100%;">
  <div class="container service">
    <center> <h2 style="padding-top: 80px">Our Services</h2>
    <div class="divider" style="margin: 0 0 70px 0;"></div></center>

    <div class="row" style="padding-bottom: 125px;">

      <div class="col-md-4 d-flex align-items-stretch mservice">
        <div class="card">
          <center><img src="images/empassion-service1.png" alt=""></center>
          <strong>INVESTMENT ADVISORY</strong>
          <div class="card-body">
            <p>We implement an integrated suite of
                solutions covering deals and transaction
                support and Performance monitoring
            </p>
                <ul>
                  <li>Financial Advisory Services (buy and
                  sell side)</li>
                  <li>Mergers and Acquisitions Services</li>
                  <li>Corporate Finance Services</li>
                </ul>

          </div>
        </div>
      </div>

      <div class="col-md-4 d-flex align-items-stretch mservice">
        <div class="card">
          <center><img src="images/empassion-service2.png" alt=""></center>
          <strong>TALENT CONSULTING</strong>
          <div class="card-body">
            <p>We help organization to works smarter
                and grow faster. We consult with our
                clients to build effective organization</p>
                <ul>
                  <li>Advisor Matchmaking</li>
                  <li>People and Organization</li>
                  <li>Training and Capability Development</li>
                  <li>Change Management</li>
                </ul>

          </div>
        </div>
      </div>

      <div class="col-md-4 d-flex align-items-stretch mservice">
        <div class="card">
          <center><img src="images/empassion-service3.png" alt=""></center>
          <strong>MANAGEMENT CONSULTING</strong>
          <div class="card-body">
            <p>Blending strategic thinking with hands-on
                practicality, we help organization to improve
                its business performance and achieve
                excellence in its operations</p>
                <ul>
                  <li>Operations Excellence</li>
                  <li>Business Process Improvement</li>
                  <li>Project Management</li>
                  <li>Business Process Mapping and SOP
                  Development</li>
                  <li>Strategic Planning</li>
                  <li>Market Entry Strategy</li>
                </ul>

          </div>
        </div>
      </div>

    </div>
  </div>
</section>




<section id="team">
  <div class="team rev">
    <center><h2>Our Team</h2>
    <div class="divider rev"></div></center><br><br>

  </div>

  <div class="container teams">
    <div class="row">

      <div class="col-md-3">
        <img src="images/DSCF8004.jpg" alt="">
      </div>
      <div class="col-md-9">
        <span class="name">Edwin Aldrin Tan</span><br>
        <span class="pos">Founding Partner</span>
        <p>He has more than 11 years of depth local and overseas experience in combined field of entrepreneurial, investment, financial management,
            organizational structuring and operational improvement areas. He co-founded and actively advising two social enterprises. He was a Vice
            President of the Investment and Business Turnaround in Capsquare Asia, a Hongkong based private equity firm, Manager in KPMG Deal
            Advisory, Head of Asia Pacific Internal Audit with some operational improvement projects in Turkey, China, Thailand and Financial consultant
            in PwC. A CFA Level 3 Candidate. He believes in improving people to achieve their best self.</p>
      </div>
    </div>

    <div class="row">
      <div class="col-md-3">
        <img src="images/DSCF7993.jpg" alt="">
      </div>
      <div class="col-md-9">
        <span class="name">Romy Cahyadi</span><br>
        <span class="pos">Founding Partner</span>
        <p>He has about 20 years in the field of social entreprenueship. He founded UnLtd Indonesia as the first incubator for social enterprises in Indonesia,
            which has supported 50+ social enterprises. He was a director at Social Enterprise International Ltd. He recently co-founded Instellar, a mission
            driven company whose purpose is to help social enterprises grow their impact and businesses to be more sustainable and scalable by incubating,
            investing and innovating with corporate, investors, government and other stakeholders. He is a co-founder of ProVisi Education, an education
            consulting company, that has helped more than 2,000 educators and more than 200 schools across the Indonesian archipelago in 20 provinces. He
            is Master of Engineering in Iowa State University.</p>
      </div>
    </div>

    <div class="row">
      <div class="col-md-3">
        <img src="images/DSCF8131.jpg" alt="">
      </div>
      <div class="col-md-9">
        <span class="name">Dian Wulandari</span><br>
        <span class="pos">Founding Partner</span>
        <p>She has more than 17 years of combined local & overseas experience in the field of consulting, research, marketing, public relation and social
            entrepeneurship. She had worked across industries and fields, -Banking, Resources, Marketing, PR & Media, Art, and Education. She was with
            HSBC, Citibank, MarkPlus, Inc., Marketeers and currently a COO of Instellar. She has a strong people management and leadership skill. She
            enjoys develop team and create systems that empower organizations to the highest level of performance. Her education background is Marketing
            Management from Universitas Padjadjaran. She believes that empowering people is the key to building the future we want.</p>
      </div>
    </div>

    <div class="row">
      <div class="col-md-3">
        <img src="images/DSCF8140.jpg" alt="">
      </div>
      <div class="col-md-9">
        <span class="name">Dian Kurniawati</span><br>
        <span class="pos">Management Consulting Head</span>
        <p>She has more than 9 years of local and overseas depth experience in management consulting, mainly focus in the area of Operations Excellence,
            Project Management, Change Management, HR, Strategy, Marketing, and Business Improvement across various sectors. She was with Accenture,
            Deloitte SEA, Philips Indonesia and Roland Berger. She has Industrial Engineering and Project Managemet education background in France. She
            has built and currently manages her own plastic recycling factory with long-term vision to turning trashes into jobs & useful stuff. She thinks the
            biggest problem we face as a species is multidimensional poverty and to end it, we need to invest in low-income to create as many sustainable jobs
            as possible.</p>
      </div>
    </div>

  </div>

</section>

  <?php require_once 'footer.php'; ?>
