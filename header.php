<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- inisialisasi css -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="style.css?v=1.1">
    <link rel="stylesheet" href="minimalize.css">
    <link rel="stylesheet" href="css/flickity.css">

    <!-- FONTS -->
    <link rel="stylesheet" href="fonts/centurygothic.ttf">
    <link rel="stylesheet" href="fonts/FuturaLT.ttf">

    <script type="text/javascript">

    </script>
    <title>Instellar</title>

</head>

<body>


<nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top" id="navbar">
    <div class="container">

        <!-- brand -->
        <a class="navbar-brand" href="#">
            <img src="images/logo.png" width="200" alt="brand">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fa fa-align-justify"></i>
        </button>

        <div class="collapse navbar-collapse right-navbar" id="navbarNav">

            <ul class="navbar-nav ml-auto">
                <li class="nav-item navbar-r-list">
                    <a class="nav-link active" href="index.php">Home</a>
                </li>
                <li class="nav-item navbar-r-list">
                    <a class="nav-link" href="#about">About Us</a>
                </li>
                <li class="navbar-item navbar-r-list">
                    <a class="nav-link" href="#services">Our Services</a>
                </li>
                <li class="navbar-item navbar-r-list">
                    <a class="nav-link" href="#team">Our Team</a>
                </li>
                <li class="nav-item navbar-r-list">
                    <a class="nav-link" href="selected-project.php">Selected Project</a>
                </li>
                <li class="nav-item navbar-r-list">
                    <a class="nav-link" href="updates.php">Updates</a>
                </li>
                <li class="nav-item navbar-r-list">
                    <a class="nav-link" href="#contact">Contact</a>
                </li>
            </ul>

        </div> <!-- /. right-navbar -->

    </div> <!-- /.container -->
</nav>
