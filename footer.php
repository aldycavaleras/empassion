<!--FOOTER  -->

<footer id="contact">
    <div class="container footer-wrapper">
        <div class="row" >

            <div class="col-md-8 rev footer">
                <img src="images/logo-full.png" width="40%" alt="">
            </div>
            <div class="col-md-4 rev footer" >
        <span class="address">
          Kolla Space 4th Floor <br>
          KH, Jl. H. Agus Salim No.32B <br>
          Kebon Sirih, Menteng,<br>
          Jakarta, 10340 <br>
          E : <a href="mailto:edwin.aldrintan@instellar.id">edwin.aldrintan@instellar.id</a><br>
          P: <a href="tel:+622150201021">+62 21 5020 1021</a><br>
          <span>
            <a href="www.empassion.id" id="web">www.empassion.id</a><br>
            </div>

        </div>
    </div>
</footer>




<!-- inisialisasi js -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://unpkg.com/scrollreveal/dist/scrollreveal.min.js"></script>
<script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script>
<script src="js/zenscroll.js"></script>
<script src="js/javascript.js"></script>




<!-- JAVASCRIPT -->

<!-- onscroll -->
<script type="text/javascript">
    // onscroll
    $(document).ready(function(){
        var scroll_start = 0;
        var startchange = $('#head-text');
        var offset = startchange.offset();
        $(document).scroll(function() {
            scroll_start = $(this).scrollTop();
            if(scroll_start > offset.top) {
                $('#navbar').css('background-color', 'transparent');
            } else {
                $('#navbar').css('background-color', 'transparent');
            }
        });
    });
</script>

<!-- carousel -->
<script type="text/javascript">

    $('.main-gallery').flickity({
        // options
        cellAlign: 'left',
        contain: true
    });

</script>

<script type="text/javascript">
    //scroll reveal
    window.sr = ScrollReveal();
    sr.reveal('.rev');
</script>


<!-- smooth scroll -->
<script>

</script>

</body>
</html>